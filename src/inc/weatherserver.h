#ifndef WEATHERSERVER_H
#define WEATHERSERVER_H

#include <QCoreApplication>

class WeatherServer : public QCoreApplication
{
    Q_OBJECT

public:
    WeatherServer(int argc, char *argv[]);
    virtual ~WeatherServer();

signals:
    void appStarted();

private slots:
    void runServer();
};

#endif // WEATHERSERVER_H
