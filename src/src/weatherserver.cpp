#include "weatherserver.h"

#include <QDebug>

WeatherServer::WeatherServer(int argc, char *argv[]) : QCoreApplication(argc, argv)
{
    QCoreApplication::setApplicationName("Weather");
    QCoreApplication::setApplicationVersion("1.0");

    connect(this, SIGNAL(appStarted()), this, SLOT(runServer()), Qt::QueuedConnection);

    emit appStarted();
}

WeatherServer::~WeatherServer() {}

void WeatherServer::runServer()
{
    qDebug()<<"It is freezing";
    exit();
}
