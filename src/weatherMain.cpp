#include <QCoreApplication>
#include <QDebug>
#include <iostream>

#include "include/weatherserver.h"

int main(int argc, char *argv[])
{
    WeatherServer server(argc, argv);

    return server.exec();
}
